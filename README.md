# Dockerized princexml 10.7

Build:

```
docker build . -t prince
```

To use, go to the directory where your `my.html` file is and run:

```
./path/to/docker-prince/run my.html
```

You can pass in whatever prince command / arguments / params you want.
