FROM ubuntu:xenial

RUN apt-get -y update \
 && apt-get -y install wget \
 && wget https://www.princexml.com/download/prince_10r7-1_ubuntu16.04_amd64.deb -O prince.deb \
 && apt-get -y install ./prince.deb \
 && rm prince.deb

WORKDIR "/data"

ENTRYPOINT ["prince"]
CMD ["--version"]
